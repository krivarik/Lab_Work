<?php
$classUnits = array(1 => "один", "два", "три", "четыре", "пятьт", "шесть", "семь", "восемь", "девять");
$classOfTens = array(1 => "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто");
$classTensAndUnits = array(1 => "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать");

function figuresWords($figures, $classUnits, $classOfTens, $classTensAndUnits)
{
    $length = strlen($figures);            /*определяет сколько знаков в числе*/
    $split = str_split($figures);          /*разбивает на составляющие числло*/
    switch ($length) {
        case 1:
            return $classUnits[$split[0]];   /*ели одинарная цифра функция возвращает ее название*/
            break;
        case 2:
            if ($figures < 20) {              /*если двойная цифра проверяем что она меньше 20*/
                return $classTensAndUnits[$split[1]];
            } elseif ($split[1] == 0) {              /*если это 20, 30, 40 и т.д. */
                return $classOfTens[$split[0]];
            } else {                                 /* если сотавная 25, 32 48 и т.д*/
                $number = $classOfTens[$split[0]];
                $number .= " ";
                $number .= $classUnits[$split[1]];
                return $number;
            }
            break;
        default:
            /* если число равно 100 возращаем значение если нет сообщаем о неизвестном числе*/
            if ($figures == 100) {
                return "сто";
            } else {
                echo "unknown number";
            }


    }

}

$figures = isset($argv[1]) ? $argv[1] : '';     /*$argv вункция принимающая данные из командной строки в виде масива где 0 имя файла. В комадной строке данные вводить через пробел*/
echo figuresWords($figures, $classUnits, $classOfTens, $classTensAndUnits);