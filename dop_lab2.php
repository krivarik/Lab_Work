<?php
/*Если в строке есть плохое слово (например, "редиска"), выведите предупреждение.

Если в строке есть плохое слово (например, "редиска"), замените его на "***".*/
/*
 *Находим в предложение плохое слово
 * Если его нет выводим предложение как оноесть
 * Если оно есть выводим сообщение и заменяем плохое слово на "***"
 */
function searchBadWords($badString, $badWord)
{
    if (!stripos($badString, $badWord) === false) {
        echo "The string contains a bad word \n";
        return str_ireplace($badWord, "****", $badString);
    } else {
        return $badString;
    }
}

$badString = "Так он же на этом скачке расколется, редиска, при первом же шухере!";
$badWord = 'редиска';
$badWord1 = "ред";
echo searchBadWords($badString, $badWord);
echo "\n";

/*а можно и посложней если к примеру нужно "ред" и только слово, а не часть другого слова*/
function searchBadWords1($badString1, $badWord1)
{
    $result=preg_match("/\b$badWord1\b/i", $badString1);
    if ($result === false) {
        echo "logical fallacy";
    } elseif ($result) {
        echo "The string contains a bad word \n";
        return preg_replace("/\b$badWord1\b/i", "****", $badString1);
    } else {
        return $badString1;
    }
}

echo searchBadWords1($badString, $badWord1);
echo "\n";


/*Если строка превышает 50 символов, выведите первые 50 символов и троеточие.
 *Если строка меньше 50 символов, оставьте ее неизменной.
 *Более сложный вариант: выведите первые 50 символов без разрыва слов.
 * */
/* Передаем в функцию Строку для форматирования и параметры форматирования, сколько символов нужно вывести
 * и чем заменить лишние, четвертый не обязательный параметр вывод первых 50 символов без разрыва слоив или нет (0=нет,
 * 1=да).
 * */
function formattedString($badString, $numberCharacters, $replace, $outputFormat = 0)
{
    $lineLength = iconv_strlen($badString); /*лучше пользоваться этой функцией она учитывает кодировку строки что очень надо для кирилицы*/
    if ($lineLength > $numberCharacters && $outputFormat == 0) {
        return iconv_substr($badString, 0, $numberCharacters) . $replace;
    } elseif ($lineLength > $numberCharacters && $outputFormat == 1) {   /* и модификатор равнялся однаму*/
        $newString = str_replace(" ", "", str_replace(",", "", $badString)); /*убираем разрывы слов*/
        $lineLength1 = iconv_strlen($newString);
        if ($lineLength1 > $numberCharacters) {                           /*проверяем чтоб строка была >50 символов*/
            return iconv_substr($newString, 0, $numberCharacters) . $replace; /*если да возвращаем модифицированную строку*/
        } else {                                                  /*если нет то выводим сообщение и возвращаем обычную строку*/
            echo "string less than 50 characters";
            return $badString;
        }
    } else {
        return $badString;
    }
}

echo formattedString($badString, "50", "...");
echo "\n";
echo formattedString($badString, "50", "...",1);
echo "\n";

/*Проверьте, является ли введенная строка валидным email*/

$validEail = "boroda@gov.com";
if (filter_var($validEail, FILTER_VALIDATE_EMAIL)) {
    echo "correct email \n";

} else {
    echo "no correct email \n";
}

/*Выведите текущую дату в формате: Четверг, 2 февраля, 2017 год.*/

echo date('l, j F, Y')." year \n";

/*или так*/
$weekDay=["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Субота" ];
$monthYear=[1=>"Января","Февраля","Марта", "Апреля", "Майя", "Июня", "Июля", "Августа", "Сентября", "Октября", "ноября", "Декабря"];

$formateDate=$weekDay[date('w')].", ".date('j')." ".$monthYear[date('n')].", ".date('Y')." год\n";
echo $formateDate;
