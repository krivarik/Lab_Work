<?php
$a=true;
$c=(int)$a; #converts boolean to integer
echo var_dump($c);
echo "\n";
$a=1;
$c=(bool)$a; /* converts positive integer to boolean*/
echo var_dump($c);
echo "\n";
$a=-1;
$c=(bool)$a; /* converts negative integer to boolean*/
echo var_dump($c);
echo "\n";
$a=0;
$c=(bool)$a; /* converts zero numbers to boolean*/
echo var_dump($c);
echo "\n";
$a=1.2;
$c=(int)$a; /* converts float to integer*/
echo var_dump($c);
echo "\n";
$a=1;
$c=(float)$a; /* converts integer in float*/
echo var_dump($c);
echo "\n";
$a="123";
$c=(int)$a; /* converts string in integer*/
echo var_dump($c);
echo "\n";
$a=123;
$c=(string)$a; /* converts integer in string*/
echo var_dump($c);
echo "\n";
$a=array(1,2,3,4,5);
$c=implode(", ", $a); // converts array in string
echo var_dump($c);
echo "\n";
$a="1,2,3,4,5";
$c=explode(",", $a); // converts string in array
echo var_dump($c);
echo "\n";