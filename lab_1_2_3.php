<?php

$_GET=array(1,2,3);
print_r($_GET);
echo "\n";
print_r($_SERVER);
echo "\n";
print_r(__FILE__); /*     rout to the script’s directory */
echo "\n";
print_r($_SERVER["SCRIPT_NAME"]);/* script file name */
echo "\n";
print_r(__LINE__); /*  number of currently executing string */
echo "\n";
/* the creation of multiple constants, verification of their existence and their values at print */
define('CONST',"apple");
const PEAR=23;
if (defined('CONST') && defined('PEAR')){
    echo constant("CONST");
    echo "\n";
    echo constant("PEAR");
}
else {
    echo "such constants does not exist";
}