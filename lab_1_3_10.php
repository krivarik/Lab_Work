<?php
$delimiter = "---------------";

function drawDelimiter()
{
    global $delimiter;
    echo $delimiter;
}

$content = "Content string";

$drawContentString = function () use ($content) {
    echo $content;
};

drawDelimiter();
echo "\n";
$drawContentString();
echo "\n";
drawDelimiter();
