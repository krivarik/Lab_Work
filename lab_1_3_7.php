﻿<?php
/*
 * функция принимает два параметра умножает и выводит результат
 */
function getSume()
{
 $args = func_get_args();
 $result = array_sum($args);
 echo $result;
}

function multiSume($sume1, $sume2)
{
 echo $sume1 * $sume2;
}

echo "Sume: ";
getSume(mt_rand(1, 10), mt_rand(8, 16), mt_rand(56, 123), mt_rand(1, 89));
echo "\n";
echo "Multi Sume: ";
multiSume(mt_rand(1, 2), mt_rand(2, 4));