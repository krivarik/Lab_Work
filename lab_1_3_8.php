<?php
function getRandomNumber($from,$to){   /*функция генерации случайных чисел*/
    return mt_rand($from,$to);
}
function getSume()  /*функция сумирования случайного набора цифр*/
{
    $args = func_get_args();
    $result = array_sum($args);
  return $result;
}

function multiSume($sume1, $sume2)  /*функция умножения двух случайных чисел*/
{
    return $sume1 * $sume2;
}
$first=getRandomNumber(1,8);
$second=getRandomNumber(9,52);
echo "Sume: ";
echo getSume(mt_rand(1, 10), mt_rand(8, 16), mt_rand(56, 123), mt_rand(1, 89));
echo "\n";
echo "Multi Sume: ";
echo multiSume($first, $second);