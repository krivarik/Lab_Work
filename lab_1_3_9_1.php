<?php
/* Функция печатает числа от 0 до 20 как только число становится  20 перестает печатать*/
function calculateFactorial($n){
    if(!$n) return 1;
    return $n * calculateFactorial($n - 1);
}
echo calculateFactorial(0); //1
echo "\n";
echo calculateFactorial(1); //1
echo "\n";
echo calculateFactorial(5); //120