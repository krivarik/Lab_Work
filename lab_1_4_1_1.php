<?php
/*var_dump($argv); *//*принимает парамтры с консоли*/
function calculator($namber1, $operation, $namber2){
    if (empty($operation)){
        echo "Error: Empty operation";
        return false;
    } 
    if (!is_numeric($namber1)|| !is_numeric($namber2)){
        echo "Error: Bad numbers";
        return false;
    }
    switch ($operation){
        case '+':
            return $namber1+$namber2;
        case '-':
            return  $namber1-$namber2;
        case '*':
            return  $namber1*$namber2;
        case '/':
            return  $namber1/$namber2;
        default:
            echo "Error: Bad operation!";
            return false;
    }
}

$namber1=isset($argv[1]) ? $argv[1]:0;
$namber2=isset($argv[3]) ? $argv[3]:0; 
$operation=isset($argv[2]) ? $argv[2]:0;

$result=calculator($namber1, $operation, $namber2);
if ($result===false){
    exit;
}

echo "{$namber1}{$operation}{$namber2}={$result}";