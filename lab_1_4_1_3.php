<?php
function find_student($username, $studens){
    if (isset($studens[$username])){

        return $studens[$username];
    }
    else{
        echo "No such user!";
        return false;
    }
}
function get_student_info_string($student_info)
{
    $result_srt = "Name :{$student_info['name']}\n";
    $result_srt .= "Age :{$student_info['age']}\n";
    $result_srt .= "Gender :{$student_info['gender']}\n";
    $result_srt .= "languages :" . implode($student_info['languages'], ", ") . "\n";
    return $result_srt;
}

$students = [
    'peter' => [
        'name' => "Peter",
        'age' => 25,
        'gender' => 'male',
        'languages' => ["php", "Java", "C++"]
    ],
    'vasya' => [
        'name' => "Vasya",
        'age' => 24,
        'gender' => 'male',
        'languages' => ["php", "Java", "C#"]
    ],
    'vova' => [
        'name' => "Vova",
        'age' => 35,
        'gender' => 'male',
        'languages' => ["php", "Java", "python"]
    ]
];

$userName = isset($argv[1]) ? $argv[1] : '';     /*$argv вункция принимающая данные из командной строки в виде масива где 0 имя файла. В комадной строке данные вводить через пробел*/
if ($student_info = find_student($userName, $students)) {
    echo get_student_info_string($student_info);
}