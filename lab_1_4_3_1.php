<?php

$min = isset($argv[1]) ? $argv[1] : 0;  /*Получает минимальное значение*/
$max = isset($argv[2]) ? $argv[2] : 0;   /*Получает максимальное значение*/
function getEvenNumbers($min, $max)     /*функция возвращает четные чисела*/
{
    if ($min < $max) {      /* Проверка направильность ввода данных. Сначала минимальное значение, затем максимальное*/
        for ($i = $min; $i <= $max; $i++) {     /*в цикле перебираем все значения от мин. до макс.*/
            $number = $i / 2;                  /*делим на 2, если делится без остатка значить четное и добавляем массив*/
            if (!is_float($number)) {
                $evenNumbers [] = $i;
            }
        }
        return implode(',', $evenNumbers);                 /*возвращаем массив из функции*/
    } else {
        echo "Invalid number specified. The first minimum number, the second maximum number";
    }

}

echo getEvenNumbers($min, $max); /*печатает строку из массива резделенного запятыми*/
