<?php

function process_params($params)
{
    $result_array = [];
    foreach ($params as $param_key => $param_value) {
        if (!$param_key) continue;
        if (substr_count($param_value, '=') != 1) {
            echo "wrong parameter: {$param_value}\n";
            continue;
        }
        $param_array = explode("=", $param_value);
        $array_count_chars = count_chars($param_array[0]); /*подсчет количества вхождений каждого символа*/
        $string_length = iconv_strlen($param_array[0]);       /* определяется длина строки (сколько символов)*/
        $array_chars = str_split($param_array[0]);          /* преобразование строки в массив*/
        
        if ($array_count_chars[45] == 1 && $string_length == 3 && $array_chars[0] == '-') { /* проверяет на соответствие - и две буквы*/
            
            $param_trim_minus = ltrim($param_array[0], "-");  /*удаляет минусы с начала строки*/
            $result_array[$param_trim_minus] = $param_array[1];
            
        } elseif ($array_count_chars[45] == 2 && $string_length > 2 && $array_chars[0] == '-' && $array_chars[1] = '-') { /*проверяет на -- и любое количество букв*/
            
            $param_trim_minus = ltrim($param_array[0], "-");  /*удаляет минусы с начала строки*/
            $result_array[$param_trim_minus] = $param_array[1];
            
        } else{
            echo "wrong parameter: {$param_value}\n";
        }
           
    }
    return $result_array;
}

$params_array=process_params($argv);

print_r($params_array);


