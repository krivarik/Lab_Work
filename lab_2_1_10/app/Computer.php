<?php
namespace app; /*если  много одинаковых методов после этого написать use "путь к файлу" as название метода*/

use exeption\ComruterException; /*чтобы не писать полный путь*/

abstract class Computer implements IComputer
{
    const IS_DESKTOP = 'true';
    private $computerName = 'Computer';
    private $cpu;
    private $ram;
    private $video;
    private $memory;
    private $isWorking;
    private $statemen = 'of';
    private $statemen_on = 'on';
    private $statemen_of = 'of';

    public function start()
    {
        if ($this->statemen == 'of') {
            $this->statemen = $this->statemen_on;
            echo "Welcome {$this->computerName} {$this->statemen}\n";
            $this->identifyUser();
        } else {
            throw new ComruterException("{$this->computerName} is on \n");
            /*echo "{$this->computerName} is on \n";*/

        }
    }

    public function shutdown()
    {
        if ($this->statemen == 'on') {
            $this->statemen = $this->statemen_of;
            echo " Good buy  {$this->computerName} {$this->statemen} \n";
        } else {
            throw new ComruterException("{$this->computerName} is of \n");
            /*echo "{$this->computerName} is of \n";*/

        }
    }

    public function restart()
    {
        if ($this->statemen == 'on') {
            $this->shutdown();
            for ($i = 0; $i < 5; $i++) {
                echo ".";
                sleep(1);

            }
            echo "\n";
            $this->start();

        }
    }

    public function printParameters()
    {
        if ($this->statemen == 'on') {
            echo "{$this->memory}\n";
            echo "{$this->cpu}\n";
            echo "{$this->ram}\n";
            echo "{$this->video}\n";
        } else {
            throw new ComruterException("{$this->computerName} is of \n");
            /*echo "{$this->computerName} is of \n";*/
        }


    }

    abstract public function identifyUser();

    public function setParam($cpu, $ram, $video, $memory)
    {
        $this->cpu = $cpu;
        $this->ram = $ram;
        $this->video = $video;
        $this->memory = $memory;
    }

    public function getComputerName()
    {
        echo $this->computerName;
    }
}