<?php
namespace app;

interface IComputer
{
    public function start();
    
    public function shutdown();
    
    public function restart();
    
    public function printParameters();

    public function identifyUser ();
}