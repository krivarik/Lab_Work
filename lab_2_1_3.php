<?php

class Computer
{
    const IS_DESKTOP = 'true';
    public $cpu;
    public $ram;
    public $video;
    public $statemen = 'of';
    public $statemen_on = 'on';
    public $statemen_of = 'of';

    function start()
    {
        if ($this->statemen == 'of') {
            $this->statemen = $this->statemen_on;
            echo "Welcome {$this->statemen}\n";
        } else {
            echo "computer is on \n";

        }
    }

    function shutdown()
    {
        if ($this->statemen == 'on') {
            $this->statemen = $this->statemen_of;
            echo " Good buy  {$this->statemen} \n";
        } else {
            echo "computer is of \n";

        }
    }

    function restart()
    {
        if ($this->statemen == 'on') {
            $this->shutdown();
            for ($i = 0; $i < 5; $i++) {
                echo ".";
                sleep(1);

            }
            echo "\n";
            $this->start();
            
        }
    }


}


$computer = new Computer();

$computer->start();
$computer->restart();
$computer->shutdown();
$computer->restart();
