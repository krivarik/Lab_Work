<?php


class Lenovo extends Computer
{
    const IS_DESKTOP = 'true';

    function __construct()
    {
        $this->cpu = "CPU: 2.8 gHz";
        $this->ram = "RAM: 2 gHz";
        $this->video = "VIDEO: 128 Mb";
        $this->memory = "MEMORY: 1 Tb";
    }

    protected function identifyUser()
    {
        echo $this->computerName;
        echo "\n Lenovo: Identify by fingerprints \n";
    }
}