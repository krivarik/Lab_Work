<?php


class MacBook extends Computer
{
    const IS_DESKTOP = 'true';
    function __construct( )
    {
        $this->cpu="CPU: 1.8 gHz";
        $this->ram="RAM: 1.5 gHz";
        $this->video="VIDEO: 256 Mb";
        $this->memory="MEMORY: 1.5 Tb";
    }
    protected function identifyUser()
    {
        echo $this->computerName;
        echo "\n MacBook: Identify by Apple ID \n";
    }
}
