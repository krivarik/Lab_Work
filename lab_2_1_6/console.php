<?php


class console
{

    static private $success = "SUCCESS";
    static private $failur = "FAILUR";
    static private $warning = "WARNING";
    static private $note = "NOTE";

    static public function printLine($message, $status){
        
        $message = (string)$message;
        $prefix = "";
        switch ($status) {
            case self::$success:
                $prefix = "Success: ";
                break;
            case self::$failur:
                $prefix = "Failur: Please tru again later";
                break;
            case self::$warning:
                $prefix = "Warning: ";
                break;

            case self::$note:
            default:
                $prefix = "";

        }
        echo $prefix.$message;
    }
}