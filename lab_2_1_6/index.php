<?php

require_once __DIR__ . "/console.php";
require_once __DIR__ . "/computer.php";

$computer = new Computer();

$computer->start();
$computer->restart();
$computer->shutdown();
$computer->restart();