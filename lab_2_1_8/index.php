<?php
require_once __DIR__."/IComputer.php";
require_once __DIR__."/Computer.php";
require_once __DIR__."/Asus.php";
require_once __DIR__."/Lenovo.php";
require_once __DIR__."/MacBook.php";

$computer = new Lenovo();

$computer->start();
$computer->printParameters();
$computer->restart();
$computer->shutdown();
$computer->restart();