<?php
namespace application;

class Lenovo extends Computer
{
    const IS_DESKTOP = 'true';

    function __construct()
    {
        $this->setParam("CPU: 2.8 gHz","RAM: 2 gHz","VIDEO: 128 Mb", "MEMORY: 1 Tb")
        /*$this->cpu = "CPU: 2.8 gHz";
        $this->ram = "RAM: 2 gHz";
        $this->video = "VIDEO: 128 Mb";
        $this->memory = "MEMORY: 1 Tb"*/;
    }

    public function identifyUser()
    {
        $this->getComputerName();
        echo "\n Lenovo: Identify by fingerprints \n";
    }
}