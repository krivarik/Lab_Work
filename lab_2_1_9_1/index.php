<?php
require_once __DIR__."/Autoload.php"; /*создаем автозагрузчик вместо кучи подключений*/
spl_autoload_register("Autoload::load_class");


$computer = new \application\Lenovo();

$computer->start();
$computer->printParameters();
$computer->restart();
$computer->shutdown();
$computer->restart();