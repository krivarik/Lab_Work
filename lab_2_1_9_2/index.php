<?php
require_once __DIR__."/Autoload.php"; /*создаем автозагрузчик вместо кучи подключений*/
spl_autoload_register("Autoload::load_class");
/*require_once __DIR__."/IComputer.php";
require_once __DIR__."/Computer.php";
require_once __DIR__."/Asus.php";
require_once __DIR__."/Lenovo.php";
require_once __DIR__."/MacBook.php";*/

$computer = new \app\Lenovo();

$computer->start();
$computer->printParameters();
$computer->restart();
$computer->shutdown();
$computer->restart();